<?php
add_action( 'rest_api_init', function () {
    register_rest_route( 'generator/v1', '/image', array(
        'methods' => 'POST',
        'callback' => array('RestClass', 'createQuestion')
    ) );
} );
