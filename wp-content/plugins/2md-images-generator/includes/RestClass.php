<?php
require_once('IMG_Generator.php');

class RestClass {

/**
 * Create question
 *
 * @param WP_REST_Request $request
 *
 * @return WP_Error|WP_REST_Response
 */
static function createQuestion( WP_REST_Request $request )
{
    $ticketNum = $request->get_param('ticket_num');

    if (!isset($ticketNum) && empty($ticketNum)) {
        return new WP_Error('400', 'Field author_id is required!');
    }

    $generator = new IMG_Generator();

    return new WP_REST_Response(array('link' => $generator->imgUrl()), 201);
}

}