<?php
/**
 * Plugin Name: 2MD Ticket Processing
 * Version:     1.0.0
 * Author:      2MD
 */

$folders = [
	'WP2MD\ProcessTicket'         => __DIR__ . '/includes',
	'Doctrine\Common\Annotations' => __DIR__ . '/lib/annotations-1.6/lib/Doctrine/Common/Annotations'
];

spl_autoload_register( function ( $class ) use ( $folders ) {
	$class = trim( $class, '\\' );

	foreach ( $folders as $namespace => $folder ) {
		if ( strpos( $class, $namespace ) !== false ) {
			$newClass = str_replace( $namespace, '', $class );
			$newClass = trim( $newClass, '\\' );

			$path = $folder;
			$path = str_replace( '/', DIRECTORY_SEPARATOR, $path );
			$path .= DIRECTORY_SEPARATOR . str_replace( '\\', DIRECTORY_SEPARATOR, $newClass );
			$path .= '.php';

			if ( is_file( $path ) ) {
				/** @noinspection PhpIncludeInspection */
				require_once $path;
			}
		}
	}
} );

try {
	@( new \WP2MD\ProcessTicket\Setup( __FILE__ ) );
} catch (\WP2MD\ProcessTicket\V1\Exception $exception) {
	add_action('admin_notices', function () use ($exception) {
		echo <<<HTML
<div class="error notice">
		<p>{$exception->getMessage()}</p>
</div>
HTML;
	});
}