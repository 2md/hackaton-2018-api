<?php
/**
 * Created by PhpStorm.
 * User: plum
 * Date: 2018-12-27
 * Time: 17:29
 */

namespace WP2MD\ProcessTicket;

use WP2MD\ProcessTicket\V1\Exception;
use WP2MD\ProcessTicket\V1\ProcessPaymentOrder;

class Setup {
	const REST_POINTS = [
		'wp2md/v1' => [
			V1\ProcessPaymentOrder::class,
			V1\OrdersFilterWithStatus::class
		]
	];

	private $roleAdmin;

	/**
	 * Setup constructor.
	 *
	 * @param $file
	 *
	 * @throws \Exception
	 */
	public function __construct( $file ) {
		$this->roleAdmin = get_role( 'administrator' );

		register_activation_hook( $file, [ $this, 'activate' ] );
		register_deactivation_hook( $file, [ $this, 'deactivate' ] );
		register_uninstall_hook( $file, [ $this, 'uninstall' ] );

		if ( static::isPluginActive() ) {
			$this->checkIfWooCommercePluginIsEnabled();

			add_action( 'rest_api_init', [ $this, 'initApi' ] );
		}
	}

	/**
	 * @throws \Exception
	 */
	public function activate() {
		$this->checkIfWooCommercePluginIsEnabled();
		if ( ! $this->roleAdmin->has_cap( ProcessPaymentOrder::CAPABILITY ) ) {
			$this->roleAdmin->add_cap( ProcessPaymentOrder::CAPABILITY );
		}
	}

	public function deactivate() {
	}

	public function uninstall() {
		if ( $this->roleAdmin->has_cap( ProcessPaymentOrder::CAPABILITY ) ) {
			$this->roleAdmin->remove_cap( ProcessPaymentOrder::CAPABILITY );
		}
	}

	public function initApi() {
		foreach ( static::REST_POINTS as $namespace => $actions ) {
			foreach ( $actions as $action ) {
				/** @noinspection PhpUndefinedMethodInspection */
				( new $action )->register( $namespace );
			}
		}
	}

	/**
	 * @throws Exception
	 */
	private function checkIfWooCommercePluginIsEnabled() {
		if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
			throw new Exception( __( 'Plugin WooCommerce is not active, is required by 2MD Ticket Processing.' ), 500, 'wp2md' );
		}
	}

	public static function isPluginActive(): bool {
		if ( ! function_exists( 'is_plugin_active' ) ) {
			/** @noinspection PhpIncludeInspection */
			require_once ABSPATH . implode( DIRECTORY_SEPARATOR, [ 'wp-admin', 'includes', 'plugin.php' ] );
		}

		return is_plugin_active( '2md-process-ticket/2md-process-ticket.php' );
	}
}
