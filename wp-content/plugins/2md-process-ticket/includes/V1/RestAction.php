<?php
/**
 * Created by PhpStorm.
 * User: plum
 * Date: 2018-12-27
 * Time: 17:45
 */

namespace WP2MD\ProcessTicket\V1;

/**
 * Class RestAction
 * @package WP2MD\ProcessTicket\V1
 */
abstract class RestAction {
	const PATH = '';
	const METHOD = '';
	const CAPABILITY = '';

	/**
	 * @var \ReflectionClass
	 */
	private $classReflection;

	/**
	 * @var \WP_REST_Request
	 */
	protected $request;

	/**
	 * @var \WP_REST_Response
	 */
	protected $response;

	/**
	 * @param string $namespace
	 *
	 * @throws \ReflectionException
	 * @throws \Exception
	 */
	public function register( string $namespace ) {
		if ( ! is_string( static::PATH ) || empty( static::PATH ) ) {
			throw new \Exception( 'Path is not defined' );
		}

		if ( strpos( \WP_REST_Server::ALLMETHODS, static::METHOD ) === false || empty( static::METHOD ) ) {
			throw new \Exception( 'Invalid method' );
		}

		$this->classReflection = new \ReflectionClass( static::class );
		if ( ! $this->classReflection->hasMethod( 'execute' ) ) {
			throw new \Exception( 'Method execute not initialize' );
		}

		$options = [
			'methods'  => static::METHOD,
			'callback' => [ $this, 'handle' ]
		];

		register_rest_route( $namespace, static::PATH, $options );
	}

	/**
	 * @param \WP_REST_Request $request
	 *
	 * @return \WP_Error|\WP_REST_Response
	 * @throws \ReflectionException
	 */
	public function handle( \WP_REST_Request $request ) {
		$this->request  = $request;
		$this->response = new \WP_REST_Response();

		try {
			$this->login();
			$this->permission();

			$params           = [];
			$methodReflection = $this->classReflection->getMethod( 'execute' );
			foreach ( $methodReflection->getParameters() as $parameter ) {
				$newParam = [ 'key' => $parameter->getPosition() ];
				$param    = $request->get_param( $parameter->getName() );

				if ( ! $param ) {
					$newParam['value'] = null;
				} elseif ( $class = $parameter->getClass() ) {
					$newParam['value'] = $class->newInstance( $param );
				} else {
					$newParam['value'] = $param;
				}

				$validationValueHandler = 'validate' . ucfirst( $parameter->getName() );
				if ( $this->classReflection->hasMethod( $validationValueHandler ) ) {
					$this->$validationValueHandler( $newParam['value'] );
				}

				$params[] = $newParam;
			}
			$params = array_column( $params, 'value', 'key' );
			ksort( $params );

			$response = $methodReflection->invokeArgs( $this, $params );
			if ($response instanceof \WP_Error) {
				return $response;
			}

			$this->response->set_data( $response );

			return $this->response;
		} /** @noinspection PhpRedundantCatchClauseInspection */ catch ( Exception $e ) {
			return new \WP_Error( $e->getNamespace(), $e->getMessage(), [ 'status' => $e->getCode() ] );
		}
	}

	/**
	 * @throws Exception
	 */
	protected function permission() {
		/** @var \WP_Error|bool $error */
		if ( ! empty( static::CAPABILITY ) && ( $error = current_user_can( static::CAPABILITY ) ) !== true ) {
			$message = __( 'You done have permission to this action' );
			if ( is_wp_error( $error ) ) {
				$message = $error->get_error_message();
			}
			throw new Exception( $message, rest_authorization_required_code(), 'wp2md' );
		}
	}

	/**
	 * @throws Exception
	 */
	protected function login() {
		if ( isset( $_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) ) {
			$user = wp_signon([
				'user_login' => $_SERVER['PHP_AUTH_USER'],
				'user_password' => $_SERVER['PHP_AUTH_PW']
			]);

			if (is_wp_error( $user)) {
				throw new Exception($user->get_error_message(), 500, 'wp2md');
			}
			
			wp_set_current_user( $user->ID);
		}
	}
}
