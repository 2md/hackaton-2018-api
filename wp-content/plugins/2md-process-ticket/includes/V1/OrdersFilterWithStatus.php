<?php
/**
 * Created by PhpStorm.
 * User: plum
 * Date: 2019-02-22
 * Time: 14:51
 */

namespace WP2MD\ProcessTicket\V1;

use OpenPayuOrderStatus;

class OrdersFilterWithStatus extends RestAction {
	const PATH = '/orders-filter-with-status/(?P<status>\w+)';
	const METHOD = \WP_REST_Server::EDITABLE;
	const CAPABILITY = 'wp2md_give_tickets';

	public function execute(array $orders, string $status) {
		return array_values(array_filter( $orders, function ($element) use ($status) {
			$order = wc_get_order($element);

			if ( ! ( $order instanceof \WC_Order ) ) {
				return false;
			}

			return $order->get_status() == strtolower($status);
		}));
	}

	/**
	 * @param string $status
	 *
	 * @throws Exception
	 */
	public function validateStatus(string $status) {
		if (!in_array( $status, [
			OpenPayuOrderStatus::STATUS_NEW,
			OpenPayuOrderStatus::STATUS_PENDING,
			OpenPayuOrderStatus::STATUS_CANCELED,
			OpenPayuOrderStatus::STATUS_REJECTED,
			OpenPayuOrderStatus::STATUS_COMPLETED,
			OpenPayuOrderStatus::STATUS_WAITING_FOR_CONFIRMATION
		])) {
			throw new Exception(__('Status is out of range'), 500, 'wp2md_process_payment');
		}
	}
}
