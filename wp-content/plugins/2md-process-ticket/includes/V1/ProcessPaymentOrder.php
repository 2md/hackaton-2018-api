<?php
/**
 * Created by PhpStorm.
 * User: plum
 * Date: 2018-12-27
 * Time: 18:08
 */

namespace WP2MD\ProcessTicket\V1;

/**
 * Class GiveAction
 * @package WP2MD\ProcessTicket\V1
 */
class ProcessPaymentOrder extends RestAction {
	const PATH = '/process-payment-order/(?P<orderId>\d+)';
	const METHOD = \WP_REST_Server::READABLE;
	const CAPABILITY = 'wp2md_give_tickets';

	/**
	 * @param int $orderId
	 *
	 * @return array|bool
	 * @throws Exception
	 */
	public function execute( int $orderId ) {
		if ( wc_get_order( $orderId ) === false ) {
			throw new Exception( __( 'Order with that id doesn\'t exists.' ), 500, 'wp2md_process_payment' );
		}

		return ( function ( \WC_Gateway_PayU $gatewayPayu, $order_id ) {
			$result = $gatewayPayu->process_payment( $order_id );
			if ( $result === false ) {
				$this->response->set_status( 500 );

				return WC()->session->get( 'wc_notices' );
			}

			return $result;
		} )( new \WC_Gateway_PayU(), $orderId );
	}

	/**
	 * @param int $id
	 *
	 * @throws Exception
	 */
	public function validateOrderId( int $id ) {
		if ( ! ( $id > 0 ) ) {
			throw new Exception( __( 'ProductId have to be greater then zero' ), 500, 'wp2md_process_payment' );
		}
	}
}
