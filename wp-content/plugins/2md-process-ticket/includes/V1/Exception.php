<?php
/**
 * Created by PhpStorm.
 * User: plum
 * Date: 2018-12-27
 * Time: 17:47
 */

namespace WP2MD\ProcessTicket\V1;

use Throwable;

class Exception extends \Exception {
	protected $namespace;

	public function __construct(string $message = "", int $code = 0, string $namespace = "", Throwable $previous = null) {
		$this->namespace = $namespace;
		parent::__construct($message, $code, $previous);
	}

	final function getNamespace() {
		return $this->namespace;
	}
}